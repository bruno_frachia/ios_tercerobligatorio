//
//  AddSongViewController.swift
//  Obligatorio3
//
//  Created by SP22 on 16/6/16.
//  Copyright © 2016 SP22. All rights reserved.
//

import UIKit

class AddSongViewController:  UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

  
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var data : [(name : String, url: NSURL)]=[]
    
    var searchText : String?
    var timer : NSTimer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchBar.placeholder = "Search artists/songs"
        self.searchBar.delegate = self
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    func searchBar (searchBar: UISearchBar, textDidChange searchText: String){
        if let timer = self.timer {
            if timer.valid {
                timer.invalidate()
            }
        }
        self.searchText = searchText
        timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("search"), userInfo: nil, repeats: false)
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("SongCell", forIndexPath: indexPath)
        cell.textLabel?.text = data[indexPath.row].name
        return cell;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let selectedSong = data[indexPath.row]
        
        let alert = UIAlertController(title: "", message: "The song has been successfully added to your list", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { action in
            let spotifyManager = SpotifyManager.sharedSpotifyManager
            spotifyManager.addSongWithURL(selectedSong.url)
        }))
        self.presentViewController(alert, animated: true, completion: nil)
 
    }
    
    
    @IBAction func backClick(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    
    func search(){
        if let searchText = self.searchText{
            self.data = []
            self.tableView.reloadData()
            
            let spotifyManager = SpotifyManager.sharedSpotifyManager
            spotifyManager.getSongsWithName(searchText) { tracksRes in
                let res : [(name : String, url: NSURL)] = tracksRes
                self.data = res
                self.tableView.reloadData()
            }
        }
        
    }
    
    

}
