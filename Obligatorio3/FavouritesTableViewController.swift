//
//  FavouritesTableViewController.swift
//  Obligatorio3
//
//  Created by Bruno Frachia on 6/30/16.
//  Copyright © 2016 SP22. All rights reserved.
//

import UIKit
import Kingfisher
import RealmSwift

class FavouritesTableViewController: UITableViewController {

    var session:SPTSession?
    var player:SPTAudioStreamingController?
    var songsArray: [Song] = []
    
    let cellReuseIdentifier = "cell"
    let cellSpacingHeight: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        let realm = try! Realm()
        
        songsArray = Array(realm.objects(Song.self))
        self.tableView.reloadData()
        if songsArray.count == 0 {
            let imageView = UIImageView(image: UIImage(named: "empty_list"))
            imageView.contentMode = .ScaleAspectFill
            self.tableView.backgroundView = imageView
        }
        else {
            self.tableView.backgroundView = UIView()
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.view.alpha = 0.5
        UIView.animateWithDuration(0.5, animations: {
            self.view.alpha = 1.0
        })
    }

    // MARK: - Table view data source

    // have one section for every array item
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.songsArray.count
    }
    
    // method to run when table view cell is tapped
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("openSong", sender: self)
        
    }

    // There is just one row in every section
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    // Set the spacing between sections
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
    // Make the background color show through
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
        return headerView
    }
    
    
    // create a cell for each table view row
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCellWithIdentifier("songCell", forIndexPath: indexPath) as! SongTableViewCell
        
        let songAtIndex : Song = self.songsArray[indexPath.section]
        
        cell.tag = indexPath.section
        
        cell.lblTitle?.text = songAtIndex.name
        
        let duration : (hour: Int, min: Int, seg: Int) = secondsToHoursMinutesSeconds(Int(songAtIndex.duration))
//        let durationSeg = duration.seg > 9 ? String(duration.seg) : "0\(duration.seg)"
        
//        cell.lblDuration.text = "\(duration.min):\(durationSeg)"
        
        cell.lblDuration.text = "\(String(format: "%01d", duration.min)):\(String(format: "%02d", duration.seg))"
        
        KingfisherManager.sharedManager.retrieveImageWithURL(NSURL(string:songAtIndex.imageUrl)!, optionsInfo: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
            
            if cell.tag == indexPath.section {
                cell.imgCover?.image  = image
            }
        })
        
        
        
        //        cell.imgCover?.image = songAtIndex.albumImage
        
        cell.imgCover?.layer.cornerRadius = 4
        
        cell.container?.layer.cornerRadius = 4
        //        cell.container?.clipsToBounds = true
        
        cell.container?.layer.shadowColor = UIColor.blackColor().CGColor
        cell.container?.layer.shadowOpacity = 0.5
        cell.container?.layer.shadowOffset = CGSize(width: 0, height: 4)
        cell.container?.layer.shadowRadius = 4
        
        
        //        cell.layer.shadowPath = UIBezierPath(rect: cell.bounds).CGPath
        
//        if isInFavourites(songAtIndex) {
//            //        if songAtIndex.isFavourite {
//            cell.favouriteIcon.image = UIImage(named: "favorite")
//        }
//        else {
//            cell.favouriteIcon.image = UIImage(named: "favorite_border")
//        }
//        
//        
//        let tap = UITapGestureRecognizer(target: self, action: Selector("favTapped:"))
//        cell.favouriteIcon.addGestureRecognizer(tap)
        
        return cell
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if(segue.identifier == "openSong") {
            
            let destinationVC = (segue.destinationViewController as! PlayerViewController)
            destinationVC.songs = self.songsArray
            destinationVC.session = self.session
            destinationVC.startSong = (self.tableView.indexPathForSelectedRow?.section)!
        }
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
