//
//  SpotifyManager.swift
//  Obligatorio3
//
//  Created by SP21 on 24/6/16.
//  Copyright © 2016 SP22. All rights reserved.
//

import Foundation
import Kingfisher

class SpotifyManager {
   
    static let sharedSpotifyManager = SpotifyManager()
    var player:SPTAudioStreamingController?

    
//    class var sharedSPTManager : SpotifyManager {
//        return sharedSpotifyManager
//    }
    
    var songs : [Song] = []
    var session : SPTSession?

    private init(){
        
    }
    
    func getSongsWithName(name: String, onCompletion: ([(name : String, url: NSURL)] -> Void)){
        var tracks : [(name : String, url: NSURL)] = []
        SPTRequest.performSearchWithQuery(name, queryType: SPTSearchQueryType.QueryTypeTrack, session: self.session) { (error: NSError!,result :AnyObject!) in
            if error == nil {
                let list = result as! SPTListPage
                if let songs = list.items{
                    for item in songs{
                        let track = item as! SPTPartialTrack
                        tracks.append((name: "\(track.name) - \(track.album.name)", track.playableUri))
                    }
                    onCompletion(tracks)
                }
            }
        }
    }
    
    
    func addSongWithURL(url: NSURL){
        
        SPTRequest.requestItemAtURI(url, withSession: self.session, callback: { (error : NSError!, result:AnyObject!) in
            if (error != nil) {
                print("*** Starting playback got error:  \(error)")
                return
            }
            let track = result as! SPTPartialTrack
   
            if let name = track.name, url = track.playableUri, albumImageURL = track.album.covers[1].imageURL {
                let song = Song()
                
                song.url = url.absoluteString
                
                song.name = name
                song.imageUrl = albumImageURL.absoluteString
                song.duration = track.duration
                
                self.songs.insert(song, atIndex: 0)

//                self.songs.append(song)
            }
            
        })

    }
    
    
    func connectSpotify() -> SPTSession?{
        let userDefaults = NSUserDefaults.standardUserDefaults()
        
        if let sessionObj = userDefaults.objectForKey("SpotifySession") {
            let sessionObj = sessionObj
            
            let session = NSKeyedUnarchiver.unarchiveObjectWithData(sessionObj as! NSData) as! SPTSession
            
            if !session.isValid(){
                // session is invalid, we must renew it
                
                SPTAuth.defaultInstance().renewSession(SPTAuth.defaultInstance().session, callback: { (error, session) in
                    if error == nil {
                        let sessionData = NSKeyedArchiver.archivedDataWithRootObject(session)
                        userDefaults.setObject(sessionData, forKey: "SpotifySession")
                        userDefaults.synchronize()
                        
                        self.session = session
                        
                    }else{
                        print("error refreshing session")
                    }
                })
            }else{
                //session valid
                print("session valid")
                self.session = session

            }
        }
        
        // Create a new player if needed
        if (SpotifyManager.sharedSpotifyManager.player == nil) {
            SpotifyManager.sharedSpotifyManager.player = SPTAudioStreamingController.init(clientId: SPTAuth.defaultInstance().clientID)
        }
        
        
        return self.session
    }
    

    func getSongs(onCompletion: (([Song]) -> Void)){
        //user first run
        if self.songs.isEmpty{
            loadInitialSongList(onCompletion)
        }else{
            onCompletion(self.songs)
        }
    }
    
    //load songs
    func loadInitialSongList(onCompletion: (([Song]) -> Void)){
//    func loadInitialSongList() -> [Song]{

        let contentURL = NSURL.init(string: "spotify:user:spotifyenespa%C3%B1ol:playlist:00moCOd4xmGozmMKwg2FXY")
        
        
        print(self.session)
        
        SPTRequest.requestItemAtURI(contentURL, withSession: self.session, callback: { (error : NSError!, result:AnyObject!) in
            if (error != nil) {
                print("*** Starting playback got error:  \(error)")
                return
            }
            let spPlayList = result as! SPTPlaylistSnapshot
            
            
            let songs = spPlayList.firstTrackPage.items
            
            var itemNumber:UInt = 0
            
            for item in songs{
                itemNumber += 1
                let track = item as! SPTPlaylistTrack
                
                if let name = track.name, url = track.playableUri, albumImageURL = track.album.covers[1].imageURL {
                    
                    let song = Song()
                    
                    song.url = url.absoluteString
                    
                    song.name = name
                    song.imageUrl = albumImageURL.absoluteString
                    song.duration = track.duration
                    
                    self.songs.append(song)
                    
                }
            }
            onCompletion(self.songs)
        })
    }
    
    
    
    func playUsingSession(song : Song) {
    
        if (SpotifyManager.sharedSpotifyManager.player!.isPlaying) {
            //song changed by user
            SpotifyManager.sharedSpotifyManager.player!.setIsPlaying(false, callback: nil)
            
            SpotifyManager.sharedSpotifyManager.player!.playURI(NSURL(string: song.url), callback: { (error) in
                if (error != nil) {
                    print("*** Starting playback got error:  \(error)")
                    return
                }
                
            })
            return
        }
        
        SpotifyManager.sharedSpotifyManager.player?.loginWithSession(self.session, callback: { (error) -> Void in
            if (error != nil) {
                print("*** Enabling playback got error: \(error)")
                return
            }
            
            SpotifyManager.sharedSpotifyManager.player!.playURI(NSURL(string: song.url), callback: { (error) in
                if (error != nil) {
                    print("*** Starting playback got error:  \(error)")
                    return
                }
            })
        })
    }


    
}