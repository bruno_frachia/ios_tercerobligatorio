//
//  SongsViewController.swift
//  Obligatorio3
//
//  Created by December on 6/12/16.
//  Copyright © 2016 SP22. All rights reserved.
//

import UIKit
import Kingfisher
import RealmSwift

class SongsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    var session:SPTSession?
    var player:SPTAudioStreamingController?
    var songsArray: [Song] = []
//    var cacheImg: [UIImage?] = []

    let cellReuseIdentifier = "cell"
    let cellSpacingHeight: CGFloat = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        let spotifyManager = SpotifyManager.sharedSpotifyManager
        spotifyManager.getSongs { songs in
            let res : [Song] = songs
            self.songsArray = res
//            self.cacheImg.reserveCapacity(res.count)
            self.tableView.reloadData()
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.view.alpha = 0.5
        UIView.animateWithDuration(0.5, animations: {
            self.view.alpha = 1.0
        })
    }
    
    // MARK: - Table View delegate methods
    
    // have one section for every array item
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.songsArray.count
    }
    
    
    // method to run when table view cell is tapped
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("openSong", sender: self)
        
    }
    
    // There is just one row in every section
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    // Set the spacing between sections
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
    // Make the background color show through
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor(red: 224/255, green: 224/255, blue: 244/255, alpha: 1)
        return headerView
    }

    // create a cell for each table view row
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCellWithIdentifier("songCell", forIndexPath: indexPath) as! SongTableViewCell
        
        let songAtIndex : Song = self.songsArray[indexPath.section]

        cell.tag = indexPath.section
        
        cell.lblTitle?.text = songAtIndex.name
        
        let duration : (hour: Int, min: Int, seg: Int) = secondsToHoursMinutesSeconds(Int(songAtIndex.duration))
        let durationSeg = duration.seg > 9 ? String(duration.seg) : "0\(duration.seg)"
        
        cell.lblDuration.text = "\(duration.min):\(durationSeg)"
      
        KingfisherManager.sharedManager.retrieveImageWithURL(NSURL(string:songAtIndex.imageUrl)!, optionsInfo: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
            
            if cell.tag == indexPath.section {
                cell.imgCover?.image  = image
            }
        })
        
        
        
//        cell.imgCover?.image = songAtIndex.albumImage
        
        cell.imgCover?.layer.cornerRadius = 4
        
        cell.container?.layer.cornerRadius = 4
//        cell.container?.clipsToBounds = true
        
        cell.container?.layer.shadowColor = UIColor.blackColor().CGColor
        cell.container?.layer.shadowOpacity = 0.5
        cell.container?.layer.shadowOffset = CGSize(width: 0, height: 4)
        cell.container?.layer.shadowRadius = 4

        
//        cell.layer.shadowPath = UIBezierPath(rect: cell.bounds).CGPath
        
        if isInFavourites(songAtIndex) {
//        if songAtIndex.isFavourite {
            cell.favouriteIcon.image = UIImage(named: "favorite")
        }
        else {
            cell.favouriteIcon.image = UIImage(named: "favorite_border")
        }
        
        
        let tap = UITapGestureRecognizer(target: self, action: Selector("favTapped:"))
        cell.favouriteIcon.addGestureRecognizer(tap)
        
        return cell
    }
    
    func favTapped(sender: UITapGestureRecognizer) {
        let tapLocation = sender.locationInView(self.tableView)
        let indexPath : NSIndexPath = self.tableView.indexPathForRowAtPoint(tapLocation)!
        
        let realm = try! Realm()
        let songAtIndex = songsArray[indexPath.section]
        let cell = self.tableView.cellForRowAtIndexPath(indexPath) as? SongTableViewCell

        if !isInFavourites(songAtIndex) {
            try! realm.write {
                realm.add(songsArray[indexPath.section].copy() as! Song)
            }
            
            UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.8, options: .CurveEaseInOut, animations: { () -> Void in
                cell?.favouriteIcon.transform = CGAffineTransformMakeScale(1.5,1.5)
                }, completion: { (finished: Bool) -> Void in
                    if finished {
                        
                        UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1, options: .CurveEaseInOut, animations: { () -> Void in
                            cell?.favouriteIcon.transform = CGAffineTransformIdentity }, completion: { (finished: Bool) -> Void in
                                
                                UIView.transitionWithView((cell?.favouriteIcon)!,
                                    duration:0.3,
                                    options: UIViewAnimationOptions.TransitionCrossDissolve,
                                    animations: {
                                        cell?.favouriteIcon.image = UIImage(named: "favorite")
                                    },
                                    completion: nil )
                                
                        })

                    }
            })
            
        }
        else {
            try! realm.write {
                realm.delete(realm.objects(Song.self).filter("url = '\(songAtIndex.url)'"))
            }
            
            UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.8, options: .CurveEaseInOut, animations: { () -> Void in
                cell?.favouriteIcon.transform = CGAffineTransformMakeScale(1.5,1.5)
                }, completion: { (finished: Bool) -> Void in
                    
                    UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1, options: .CurveEaseInOut, animations: { () -> Void in
                        cell?.favouriteIcon.transform = CGAffineTransformIdentity }, completion: { (finished: Bool) -> Void in
            
                            UIView.transitionWithView((cell?.favouriteIcon)!,
                                duration:0.3,
                                options: UIViewAnimationOptions.TransitionCrossDissolve,
                                animations: {
                                    cell?.favouriteIcon.image = UIImage(named: "favorite_border")
                                },
                                completion: nil )

                            })
            })
        }

    }

    func isInFavourites(song: Song) -> Bool {
    
        // Open the Realm with the configuration
        let realm = try! Realm()
        
        // Read some data from the bundled Realm
        let results = realm.objects(Song.self).filter("url = '\(song.url)'")
        
        if results.count > 0 {
            return true
        }
        return false
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if(segue.identifier == "openSong") {
            
            let destinationVC = (segue.destinationViewController as! PlayerViewController)
            destinationVC.songs = self.songsArray
            destinationVC.session = self.session
            destinationVC.startSong = (self.tableView.indexPathForSelectedRow?.section)!
        }
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
}
