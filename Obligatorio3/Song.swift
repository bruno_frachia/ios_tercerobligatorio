//
//  Song.swift
//  Obligatorio3
//
//  Created by December on 6/12/16.
//  Copyright © 2016 SP22. All rights reserved.
//

import Foundation
import RealmSwift

class Song : Object {
    
    dynamic var url = ""
    dynamic var name = ""
    dynamic var imageUrl = ""
    dynamic var duration = 0.0
    dynamic var isFavourite = false
    
//    init(url: NSURL, name:String, albumImage:UIImage, duration: Double){
//        self.url = url
//        self.name = name
//        self.albumImage = albumImage
//        self.duration = duration
//    }
    
    func copyWithZone(zone: NSZone) -> AnyObject {
        let copy = Song()
        copy.url = self.url
        copy.name = self.name
        copy.imageUrl = self.imageUrl
        copy.duration = self.duration
        copy.isFavourite = self.isFavourite
        return copy
    }
    
}
