//
//  PlayerViewController.swift
//  Obligatorio3
//
//  Created by SP22 on 17/6/16.
//  Copyright © 2016 SP22. All rights reserved.
//

import UIKit
import iCarousel
import Kingfisher

class PlayerViewController: UIViewController, iCarouselDataSource, iCarouselDelegate, SPTAudioStreamingPlaybackDelegate {

    var songs = [Song]()
    
    var startSong: Int?
    
    var session:SPTSession?
    
    var spotifyManager: SpotifyManager = SpotifyManager.sharedSpotifyManager
    
    @IBOutlet weak var carousel: iCarousel!
    
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var imgBack: UIImageView!
    
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var btnPlay: UIView!
    @IBOutlet weak var imgPlay: UIImageView!
    @IBOutlet weak var imgPause: UIImageView!
    
    @IBOutlet weak var prevContainer: UIView!
    @IBOutlet weak var btnPrev: UIView!
    
    @IBOutlet weak var nextContainer: UIView!
    @IBOutlet weak var btnNext: UIView!
    
    @IBOutlet weak var progressSlider: UISlider!
    @IBOutlet weak var currentPositionLabel: UILabel!
    @IBOutlet weak var songDurationLabel: UILabel!
    
    //to check the current track progress
    var timer = NSTimer()
    
    var isPlaying = true {
        didSet {
            changeBtnIcon()
        }
    }
    
    var didCurrentSongStarted = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(PlayerViewController.updateProgress), userInfo: nil, repeats: true)
        
        carousel.type = .CoverFlow
        
        carousel.scrollToItemAtIndex(startSong!, animated: false)
        
        self.styleViews()
        
        if startSong == 0 {
            carouselCurrentItemIndexDidChange(carousel)
        }
    }
   
    
    func updateProgress() {
        if let currentPos = self.spotifyManager.player?.currentPlaybackPosition {
            if let currentDur = self.spotifyManager.player?.currentTrackDuration {
                progressSlider.setValue(Float(currentPos/currentDur), animated: true)
                let position = secondsToHoursMinutesSeconds(Int(currentPos))
                currentPositionLabel.text = "\(String(format: "%01d", position.1)):\(String(format: "%02d", position.2))"
            }
        }
    }
    
    
    //MARK: - Carousel methods
    func numberOfItemsInCarousel(carousel: iCarousel) -> Int {
        return self.songs.count
    }
    
    func carousel(carousel: iCarousel, viewForItemAtIndex index: Int, reusingView view: UIView?) -> UIView {
        
        var itemView: UIImageView
        if (view == nil) {
            itemView = UIImageView(frame:CGRect(x:0, y:0, width:200, height:200))
            itemView.contentMode = .ScaleAspectFit
        }
        else {
            itemView = view as! UIImageView;
        }
        
        KingfisherManager.sharedManager.retrieveImageWithURL(NSURL(string:songs[index].imageUrl)!, optionsInfo: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
            itemView.image = image
        })
        
        return itemView
    }
    
    func carousel(carousel: iCarousel, valueForOption option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if option == iCarouselOption.Spacing {
            return value * 3
        }
        return value
    }
    
    func carouselCurrentItemIndexDidChange(carousel: iCarousel) {
        
        didCurrentSongStarted = false
        playCurrentSong()
        
        KingfisherManager.sharedManager.retrieveImageWithURL(NSURL(string:self.songs[self.carousel.currentItemIndex].imageUrl)!, optionsInfo: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
            
            UIView.transitionWithView(self.imgBack,
                duration:1,
                options: UIViewAnimationOptions.TransitionCrossDissolve,
                animations: {
                    self.imgBack.image = image
                },
                completion: nil )
        })
        
    }
    
    @IBAction func sliderValueChanged(sender: UISlider) {
        
        if let currentDur = self.spotifyManager.player?.currentTrackDuration {
            let offset = currentDur * Double(sender.value)
            self.spotifyManager.player?.seekToOffset(offset, callback: nil)
        }
        
    }
    @IBAction func onPlayTapped(sender: AnyObject) {
        playCurrentSong()
    }

    @IBAction func onPrevTapped(sender: AnyObject) {
        self.carousel.scrollToItemAtIndex(self.carousel.currentItemIndex-1, animated: true)
    }
    @IBAction func onNextTapped(sender: AnyObject) {
        self.carousel.scrollToItemAtIndex(self.carousel.currentItemIndex+1, animated: true)
    }
    @IBAction func dismissAction(sender: AnyObject) {
        self.isPlaying = false
        self.didCurrentSongStarted = false
        self.spotifyManager.player?.setIsPlaying(false, callback: nil)
        self.spotifyManager.player?.stop(nil)
        self.spotifyManager.player?.logout(nil)
        self.dismissViewControllerAnimated(true, completion: nil)

    }
    
    func styleViews() {
        
        self.btnClose.layer.shadowColor = UIColor.blackColor().CGColor
        self.btnClose.layer.shadowOffset = CGSizeZero
        self.btnClose.layer.shadowOpacity = 1
        self.btnClose.layer.shadowRadius = 8
        
        self.shadowView.layer.shadowColor = UIColor.blackColor().CGColor
        self.shadowView.layer.shadowOffset = CGSizeZero
        self.shadowView.layer.shadowOpacity = 0.5
        self.shadowView.layer.shadowRadius = 5
        
        self.btnPlay.layer.cornerRadius = btnPlay.frame.size.height/2
        self.btnPlay.clipsToBounds = true
        
        
        self.prevContainer.layer.shadowColor = UIColor.blackColor().CGColor
        self.prevContainer.layer.shadowOffset = CGSizeZero
        self.prevContainer.layer.shadowOpacity = 0.5
        self.prevContainer.layer.shadowRadius = 5
        
        self.btnPrev.layer.cornerRadius = btnPrev.frame.size.height/2
        self.btnPrev.clipsToBounds = true
        
        
        self.nextContainer.layer.shadowColor = UIColor.blackColor().CGColor
        self.nextContainer.layer.shadowOffset = CGSizeZero
        self.nextContainer.layer.shadowOpacity = 0.5
        self.nextContainer.layer.shadowRadius = 5
        
        self.btnNext.layer.cornerRadius = btnNext.frame.size.height/2
        self.btnNext.clipsToBounds = true
    }
    


    
    func playCurrentSong() {
        
        if self.didCurrentSongStarted {
            if self.isPlaying {
                self.isPlaying = false
                self.spotifyManager.player?.setIsPlaying(false, callback: nil)
            }
            else {
                self.isPlaying = true
                self.spotifyManager.player?.setIsPlaying(true, callback: nil)
            }
        }
        else {
            self.isPlaying = true
            let songDuration = secondsToHoursMinutesSeconds(Int(songs[carousel.currentItemIndex].duration))
            songDurationLabel.text = "\(String(format: "%01d", songDuration.1)):\(String(format: "%02d", songDuration.2))"
            self.didCurrentSongStarted = true
            self.spotifyManager.playUsingSession(songs[carousel.currentItemIndex])
        }
    }
    
    func changeBtnIcon() {
        if isPlaying {
            imgPlay.hidden = true
            imgPause.hidden = false
        }
        else {
            imgPlay.hidden = false
            imgPause.hidden = true
        }
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
}
