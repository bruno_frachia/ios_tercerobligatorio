//
//  PlayerViewController.swift
//  Obligatorio3
//
//  Created by December on 6/11/16.
//  Copyright © 2016 SP22. All rights reserved.
//

import UIKit



class LoginViewController: UIViewController {

    
    let kClidentId = "576ba13451304e0da45cab7ece49569c"
    let kCallbackURL = "obligatorio3://returnafterlogin"
  //  let tokenSwapURL = "http://localhost:1234/swap"
   // let tokenRefreshURL = "http://localhost:1234/refresh"
   
  //  var session:SPTSession!
    
    var spotifyManager : SpotifyManager?
    
    var session: SPTSession?
    
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    
        self.spotifyManager = SpotifyManager.sharedSpotifyManager
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updateAfterFirstLogin", name: "loginSuccessfull", object: nil)
//        if self.session == nil{
//            self.session = spotifyManager!.connectSpotify()
//        }else{
//            updateAfterFirstLogin()
//        }
        
        if let _ = self.session{
            updateAfterFirstLogin()
        }
        
    }
    
    
//    func connectSpotify(){
//        let userDefaults = NSUserDefaults.standardUserDefaults()
//        
//        if let sessionObj = userDefaults.objectForKey("SpotifySession") {
//            let sessionObj = sessionObj
//            
//            let session = NSKeyedUnarchiver.unarchiveObjectWithData(sessionObj as! NSData) as! SPTSession
//            
//            
//            
//            if !session.isValid(){
//                // session is invalid, we must renew it
//                
//                SPTAuth.defaultInstance().renewSession(SPTAuth.defaultInstance().session, callback: { (error, session) in
//                    if error == nil {
//                        let sessionData = NSKeyedArchiver.archivedDataWithRootObject(session)
//                        userDefaults.setObject(sessionData, forKey: "SpotifySession")
//                        userDefaults.synchronize()
//                        
//                        self.session = session
//                       // self.performSegueWithIdentifier("SongsSegue", sender: session)
//                        self.performSegueWithIdentifier("SongsSegue", sender: nil)
//
//                        //keep valid session
//                        // self.playUsingSession(session)
//                        
//                    }else{
//                        print("error refreshing session")
//                    }
//                })
//            }else{
//                //session valid
//                
//                print("session valid")
//                self.session = session
//                self.performSegueWithIdentifier("SongsSegue", sender: nil)
//                //self.playUsingSession(session)
//                
//            }
//        }
//    }
    
    
    
    func updateAfterFirstLogin(){
        if let session = self.spotifyManager!.connectSpotify(){
            self.session = session
            self.performSegueWithIdentifier("SongsSegue", sender: nil)
        }

    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "SongsSegue"){
            let barViewControllers = segue.destinationViewController as! UITabBarController
            
            let destinationNavController =  barViewControllers.viewControllers![0] as! UINavigationController
            
            let targetController = destinationNavController.topViewController as! SongsViewController
            
            let spotifySession = self.session
            
            targetController.session = spotifySession
            
        }
    }
    
    
    
    
    @IBAction func loginWithSpotify(sender: AnyObject) {
        
        let auth = SPTAuth.defaultInstance()
        
        auth.clientID = kClidentId
        auth.redirectURL = NSURL.init(string: kCallbackURL)
  //      auth.tokenSwapURL = NSURL.init(string: tokenSwapURL)
  //      auth.tokenRefreshURL = NSURL.init(string: tokenRefreshURL)
        auth.requestedScopes = [SPTAuthStreamingScope ]
        
        let loginURL = auth.loginURL
        
        UIApplication.sharedApplication().openURL(loginURL)
      

        
    }
    

    
}
