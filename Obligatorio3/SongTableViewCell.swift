//
//  SongTableViewCell.swift
//  Obligatorio3
//
//  Created by SP22 on 15/6/16.
//  Copyright © 2016 SP22. All rights reserved.
//

import UIKit

class SongTableViewCell: UITableViewCell {

    @IBOutlet weak var container: UIView!
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var favouriteIcon: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        self.container.backgroundColor = UIColor(red: 206, green: 206, blue: 206, alpha: 1)
    }

}
