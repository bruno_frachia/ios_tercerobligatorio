//
//  SongCollectionViewCell.swift
//  Obligatorio3
//
//  Created by SP22 on 17/6/16.
//  Copyright © 2016 SP22. All rights reserved.
//

import UIKit

class SongCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var btnPlay: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        shadowView.layer.shadowColor = UIColor.blackColor().CGColor
        shadowView.layer.shadowOffset = CGSizeZero
        shadowView.layer.shadowOpacity = 0.5
        shadowView.layer.shadowRadius = 5
        
        btnPlay.layer.cornerRadius = btnPlay.frame.size.height/2
        btnPlay.clipsToBounds = true
        
    }
    
    
}
